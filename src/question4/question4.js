import React from "react";
import { Button, Question4StyleWapper } from "./styleQuestion";

const Box = ({ score = "" }) => {
  return (
    <div className="box">
      <h1>{score}</h1>
      <span>Posts</span>
    </div>
  );
};

export default function Question4() {
  return (
    <Question4StyleWapper>
      <div className="secound-square">
        <div className="column-first">
          <div className="img-profile" />
          <div className="text-container">
            <h1>Pupae</h1>
            <span>Vis test some</span>
          </div>

          <Button>Follow</Button>
          <Button>Message</Button>
        </div>
        <div className="box-container">
          <Box score="1000" />
          <Box score="576" />
          <Box score="222222" />
        </div>
      </div>
    </Question4StyleWapper>
  );
}
