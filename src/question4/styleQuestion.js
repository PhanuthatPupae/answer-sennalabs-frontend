import styled from "styled-components";

export const Question4StyleWapper = styled.div`
  background: linear-gradient(
    90deg,
    rgba(234, 182, 106, 1) 0%,
    rgba(204, 130, 80) 80%
  );
  width: 800px;
  height: 600px;
  align-items: center;
  justify-content: center;
  display: flex;
  .secound-square {
    width: 700px;
    height: 500px;
    align-items: center;
    justify-content: center;
    display: flex;
    background-color: #fff;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.3), 0 6px 20px 0 rgba(0, 0, 0, 0.2);
    .column-first {
      width: 100%;
      height: 100%;
      justify-content: center;
      align-items: center;
      display: flex;
      flex-direction: column;
      .img-profile {
        border-radius: 50px;
        width: 120px;
        height: 120px;
        background-color: snow;
      }
      .text-container {
        margin-bottom: 5em;
        margin-top: 2em;
        h1 {
          color: #8b7d74;
          margin: 0;
        }
        span {
          color: #ebebeb;
        }
      }
    }
  }
  .box-container {
    display: flex;
    width: 70%;
    height: 100%;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    gap: 2px;
    .box {
      background-color: #f5e8df;
      display: flex;
      width: 100%;
      height: 100%;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      h1 {
        color: #8b7d74;
        margin: 0;
      }
      span {
        color: #fff;
      }
    }
  }
`;

export const Button = styled.button`
  width: 200px;
  height: 50px;
  border: 2px solid #8b7d74;
  border-radius: 50px;
  align-items: center;
  justify-content: center;
  display: flex;
  background-color: #fff;
  font-weight: bold;
  font-size: 18px;
  color: #8b7d74;
  margin-top: 10px;
  margin-bottom: 10px;
  &:focus {
    outline: none;
  }
`;
