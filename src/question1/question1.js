import React, { useState, useEffect } from "react";

const exampleData = [
  { name: "rick", cars: ["Corvette Z06", "Lotus Exite S"] },
  { name: "john", cars: ["BMW 320D"] },
  { name: "zing", cars: ["Honda Jazz", "Honda Click", "Honda Waves"] },
];

const sortListNameByAlphabet = (data = []) => {
  const sortNameByASC = data.sort((firstData, secondData) => {
    if (firstData && secondData && secondData.name && firstData.name) {
      if (firstData.name > secondData.name) {
        return 1;
      }
      if (firstData.name < secondData.name) {
        return -1;
      } else {
        return 0;
      }
    }
  });
  return sortNameByASC;
};

const strFirstUpperCase = (words = "") => {
  return words.slice(0, 1).toUpperCase() + words.slice(1);
};

const textWithCommas = (texts = []) => {
  const length = texts.length;
  return texts.map((t, i) => (i === length - 1 ? t : t + ", "));
};

const renderNameWithCars = (data = []) => {
  return data.map((d) => {
    return (
      <div style={{ flexDirection: "row" }}>
        <p>
          {strFirstUpperCase(d.name)} want to buy {textWithCommas(d.cars)}
        </p>
      </div>
    );
  });
};

export default function Question1() {
  const [data, setData] = useState([]);
  useEffect(() => {
    const dataAfterSort = sortListNameByAlphabet(exampleData);
    setData(dataAfterSort);
  }, []);
  return <div>{renderNameWithCars(data)}</div>;
}
